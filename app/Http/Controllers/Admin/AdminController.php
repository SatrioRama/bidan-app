<?php

namespace App\Http\Controllers\Admin;

use App\Models\Consultation;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public $data = []; // the information we send to the view

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $this->data['title'] = trans('backpack::base.dashboard'); // set the page title
        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin')     => backpack_url('dashboard'),
            trans('backpack::base.dashboard') => false,
        ];

        if (backpack_user()->type == 'bidan') {
            $this->data['data'] = Consultation::get();
            return view('dashboard.bidan', $this->data);
        }else {
            $this->data['data'] = Consultation::where('pasien_email', '=', backpack_user()->email)->get();
            return view('dashboard.pasien', $this->data);
        }
    }

}
