<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BookingRequest;
use App\Models\Booking;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BookingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BookingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Booking::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/booking');
        CRUD::setEntityNameStrings('booking', 'bookings');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        if (backpack_user()->type == 'bidan') {
            $this->crud->removeButton('create');
        }else {
            $this->crud->addClause('where', 'pasien_email', '=', backpack_user()->email);
        }
        CRUD::column('pasien_name');
        CRUD::column('pasien_email');
        // CRUD::column('category_id');
        CRUD::column('booking_time');
        CRUD::column('complaint');
        CRUD::column('confirmed');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BookingRequest::class);

        $this->crud->addField([   // Hidden
            'name'  => 'pasien_id',
            'type'  => 'hidden',
            'value' => backpack_user()->id,
        ]);
        $this->crud->addField([   // Text
            'name'  => 'pasien_name',
            'label' => "Nama",
            'type'  => 'text',
            'value' => backpack_user()->name,

            // optional
            //'prefix'     => '',
            //'suffix'     => '',
            //'default'    => 'some value', // default value
            //'hint'       => 'Some hint text', // helpful text, show up after input
            //'attributes' => [
               //'placeholder' => 'Some text when empty',
               //'class' => 'form-control some-class',
               //'readonly'  => 'readonly',
               //'disabled'  => 'disabled',
             //], // extra HTML attributes and values your input might need
             //'wrapper'   => [
               //'class' => 'form-group col-md-12'
             //], // extra HTML attributes for the field wrapper - mostly for resizing fields

        ]);
        if (backpack_user()->type == 'bidan') {
            $this->crud->addField([   // Text
                'name'  => 'pasien_email',
                'label' => "Email",
                'type'  => 'text',

                // optional
                //'prefix'     => '',
                //'suffix'     => '',
                //'default'    => 'some value', // default value
                //'hint'       => 'Some hint text', // helpful text, show up after input
                'attributes' => [
                   //'placeholder' => 'Some text when empty',
                   //'class' => 'form-control some-class',
                   'readonly'  => 'readonly',
                   //'disabled'  => 'disabled',
                 ], // extra HTML attributes and values your input might need
                 //'wrapper'   => [
                   //'class' => 'form-group col-md-12'
                 //], // extra HTML attributes for the field wrapper - mostly for resizing fields

            ]);
        } else {
            $this->crud->addField([   // Text
                'name'  => 'pasien_email',
                'label' => "Email",
                'type'  => 'text',
                'value' => backpack_user()->email,

                // optional
                //'prefix'     => '',
                //'suffix'     => '',
                //'default'    => 'some value', // default value
                //'hint'       => 'Some hint text', // helpful text, show up after input
                'attributes' => [
                   //'placeholder' => 'Some text when empty',
                   //'class' => 'form-control some-class',
                   'readonly'  => 'readonly',
                   //'disabled'  => 'disabled',
                 ], // extra HTML attributes and values your input might need
                 //'wrapper'   => [
                   //'class' => 'form-group col-md-12'
                 //], // extra HTML attributes for the field wrapper - mostly for resizing fields

            ]);
        }
        $this->crud->addField(
            [  // Select2
                'label'     => "Kategori Konsultasi",
                'type'      => 'select2',
                'name'      => 'category_id', // the db column for the foreign key

                // optional
                'entity'    => 'category', // the method that defines the relationship in your Model
                'model'     => "App\Models\Category", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                // 'default'   => 2, // set the default value of the select2

                 // also optional
                // 'options'   => (function ($query) {
                //      return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                //  }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
             ]
        );
        $this->crud->addField([
            'name'       => 'booking_time',
            'label'      => 'Waktu dan Tanggal Kunjungan',
            'type'       => 'datetime_picker',
            'allows_null' => true,
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'id',
                'minDate' => date('Y-m-d H:i'),
                'tooltips' => [
                    'today' => 'Hoje',
                    'selectDate' => 'Pilih waktu',
                ],
            ],
        ]);
        $this->crud->addField([   // Textarea
            'name'  => 'complaint',
            'label' => 'Keluhan Awal',
            'type'  => 'textarea'
        ]);
        if (backpack_user()->type == 'bidan') {
            CRUD::field('confirmed');
        }else{
            $this->crud->addField([   // Hidden
                'name'  => 'confirmed',
                'type'  => 'hidden',
                'value' => false,
            ]);
        }

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(BookingRequest::class);

        $this->crud->addField([   // Hidden
            'name'  => 'pasien_id',
            'type'  => 'hidden',
        ]);
        $this->crud->addField([   // Text
            'name'  => 'pasien_name',
            'label' => "Nama",
            'type'  => 'text',

            // optional
            //'prefix'     => '',
            //'suffix'     => '',
            //'default'    => 'some value', // default value
            //'hint'       => 'Some hint text', // helpful text, show up after input
            //'attributes' => [
               //'placeholder' => 'Some text when empty',
               //'class' => 'form-control some-class',
               //'readonly'  => 'readonly',
               //'disabled'  => 'disabled',
             //], // extra HTML attributes and values your input might need
             //'wrapper'   => [
               //'class' => 'form-group col-md-12'
             //], // extra HTML attributes for the field wrapper - mostly for resizing fields

        ]);
        if (backpack_user()->type == 'bidan') {
            $this->crud->addField([   // Text
                'name'  => 'pasien_email',
                'label' => "Email",
                'type'  => 'text',

                // optional
                //'prefix'     => '',
                //'suffix'     => '',
                //'default'    => 'some value', // default value
                //'hint'       => 'Some hint text', // helpful text, show up after input
                'attributes' => [
                   //'placeholder' => 'Some text when empty',
                   //'class' => 'form-control some-class',
                   'readonly'  => 'readonly',
                   //'disabled'  => 'disabled',
                 ], // extra HTML attributes and values your input might need
                 //'wrapper'   => [
                   //'class' => 'form-group col-md-12'
                 //], // extra HTML attributes for the field wrapper - mostly for resizing fields

            ]);
        } else {
            $this->crud->addField([   // Text
                'name'  => 'pasien_email',
                'label' => "Email",
                'type'  => 'text',
                'value' => backpack_user()->email,

                // optional
                //'prefix'     => '',
                //'suffix'     => '',
                //'default'    => 'some value', // default value
                //'hint'       => 'Some hint text', // helpful text, show up after input
                'attributes' => [
                   //'placeholder' => 'Some text when empty',
                   //'class' => 'form-control some-class',
                   'readonly'  => 'readonly',
                   //'disabled'  => 'disabled',
                 ], // extra HTML attributes and values your input might need
                 //'wrapper'   => [
                   //'class' => 'form-group col-md-12'
                 //], // extra HTML attributes for the field wrapper - mostly for resizing fields

            ]);
        }
        $this->crud->addField(
            [  // Select2
                'label'     => "Kategori Konsultasi",
                'type'      => 'select2',
                'name'      => 'category_id', // the db column for the foreign key

                // optional
                'entity'    => 'category', // the method that defines the relationship in your Model
                'model'     => "App\Models\Category", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                // 'default'   => 2, // set the default value of the select2

                 // also optional
                // 'options'   => (function ($query) {
                //      return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                //  }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
             ]
        );
        $this->crud->addField([
            'name'       => 'booking_time',
            'label'      => 'Waktu dan Tanggal Kunjungan',
            'type'       => 'datetime_picker',
            'allows_null' => true,
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'id',
                'minDate' => date('Y-m-d H:i'),
                'tooltips' => [
                    'today' => 'Hoje',
                    'selectDate' => 'Pilih waktu',
                ],
            ],
        ]);
        $this->crud->addField([   // Textarea
            'name'  => 'complaint',
            'label' => 'Keluhan Awal',
            'type'  => 'textarea'
        ]);
        if (backpack_user()->type == 'bidan') {
            CRUD::field('confirmed');
        }else{
            $this->crud->addField([   // Hidden
                'name'  => 'confirmed',
                'type'  => 'hidden',
                'value' => false,
            ]);
        }
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // insert item in the db
        $item = Booking::create($request->all());
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }

    public function update($id)
    {
        $this->crud->hasAccessOrFail('update');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest('update', $id);

        // find the item in the database
        $item = Booking::findOrFail($id);

        // update item in the db
        $item->update($request->all());

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($id);
    }

}
