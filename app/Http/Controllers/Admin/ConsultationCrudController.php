<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ConsultationRequest;
use App\Models\Consultation;
use App\Models\Medicine;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\URL;

/**
 * Class ConsultationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ConsultationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Consultation::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/consultation');
        CRUD::setEntityNameStrings('consultation', 'consultations');
        CRUD::setCreateView('consultation.create');
        CRUD::setEditView('consultation.edit');
        CRUD::setShowView('consultation.show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        if (backpack_user()->type != 'bidan') {
            $this->crud->removeButtons(['create', 'update', 'delete']);
            $this->crud->addClause('where', 'pasien_email', '=', backpack_user()->email);
        }
        CRUD::column('pasien_email');
        CRUD::column('category_id');
        CRUD::column('initial_diagnosis');
        CRUD::column('description');
        $this->crud->addColumn([
            // any type of relationship
            'name'         => 'medicines', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Medicines', // Table column heading
            // OPTIONAL
            // 'entity'    => 'tags', // the method that defines the relationship in your Model
            // 'attribute' => 'name', // foreign key attribute that is shown to user
            // 'model'     => App\Models\Category::class, // foreign key model
        ]);

        $this->crud->addColumn([
            'name'     => 'price',
            'label'    => 'Total Amount',
            'type'     => 'closure',
            'function' => function($entry) {
                $total_med = 0;
                foreach ($entry->medicines as $key => $medicine) {
                    $total_med += $medicine->pivot->quantity*$medicine->price;
                }
                return "Rp. ".number_format($total_med+$entry->category->price, 0, ',', '.');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ConsultationRequest::class);

        CRUD::field('booking_id');
        CRUD::field('pasien_email');
        CRUD::field('category_id');
        CRUD::field('initial_diagnosis');
        CRUD::field('description');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(ConsultationRequest $request)
    {
        $consultation = new Consultation();
        $consultation->booking_id = $request->input('booking_id');
        $consultation->pasien_email = $request->input('pasien_email');
        $consultation->category_id = $request->input('category_id');
        $consultation->initial_diagnosis = $request->input('initial_diagnosis');
        $consultation->description = $request->input('description');
        $consultation->bidan_id = backpack_user()->id;
        $consultation->save();

        // Sync medicines for the consultation
        if (!empty($request->input('medicines'))) {
            $this->attachMedicinesToConsultation($consultation, $request->input('medicines'), $request->input('quantities'));
            foreach ($request->input('medicines') as $key => $med_id) {
                $medicine = Medicine::find($med_id);
                $medicine->stock -= $request->input('quantities')[$key];
                $medicine->update();
            }
        }

        // Flash a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $showUrl = URL::route('consultation.show', ['id' => $consultation->id]);

        // Redirect to the preview page
        return redirect()->to($showUrl);
    }

    public function update(ConsultationRequest $request, $id)
    {
        $consultation = Consultation::findOrFail($id);

        $originalQuantities = [];
        foreach ($consultation->medicines as $medicine) {
            $originalQuantities[$medicine->id] = $medicine->pivot->quantity;
        }

        foreach ($originalQuantities as $med_id => $originalQty) {
            $medicine = Medicine::find($med_id);
            $medicine->stock += $originalQty;
            $medicine->update();
        }
        $consultation->booking_id = $request->input('booking_id');
        $consultation->pasien_email = $request->input('pasien_email');
        $consultation->category_id = $request->input('category_id');
        $consultation->initial_diagnosis = $request->input('initial_diagnosis');
        $consultation->description = $request->input('description');
        $consultation->bidan_id = backpack_user()->id;
        $consultation->save();

        // Sync medicines for the
        if (!empty($request->input('medicines'))) {
            $this->syncMedicinesForConsultation($consultation, $request->input('medicines'), $request->input('quantities'));
            foreach ($request->input('medicines') as $key => $med_id) {
                $medicine = Medicine::find($med_id);
                $medicine->stock -= $request->input('quantities')[$key];
                $medicine->update();
            }
        }

        // Flash a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $showUrl = URL::route('consultation.show', ['id' => $id]);

        // Redirect to the preview page
        return redirect()->to($showUrl);
    }

    private function attachMedicinesToConsultation(Consultation $consultation, $medicineIds, $quantities)
    {
        $medicines = Medicine::whereIn('id', $medicineIds)->get();

        $medicinesWithQuantities = [];
        foreach ($medicines as $index => $medicine) {
            $medicinesWithQuantities[$medicine->id] = ['quantity' => $quantities[$index]];
        }

        $consultation->medicines()->attach($medicinesWithQuantities);
    }

    private function syncMedicinesForConsultation(Consultation $consultation, $medicineIds, $quantities)
    {
        $medicines = Medicine::whereIn('id', $medicineIds)->get();

        $medicinesWithQuantities = [];
        foreach ($medicines as $index => $medicine) {
            $medicinesWithQuantities[$medicine->id] = ['quantity' => $quantities[$index]];
        }

        $consultation->medicines()->sync($medicinesWithQuantities);
    }

    public function printReceipt($id)
    {
        // Retrieve the consultation data from the database
        $consultation = Consultation::findOrFail($id);

        // You can customize the receipt view based on your requirements
        return view('consultation.receipt', compact('consultation'));
    }

}
