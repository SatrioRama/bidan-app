<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MedicineRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MedicineCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MedicineCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Medicine::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/medicine');
        CRUD::setEntityNameStrings('medicine', 'medicines');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'name', // The db column name
            'label'     => 'Nama Obat', // Table column heading
            // 'prefix' => 'Name: ',
            // 'suffix' => '(user)',
            // 'limit'  => 120, // character limit; default is 50,
         ]);
        $this->crud->addColumn([
            'name'  => 'stock', // The db column name
            'label' => 'Stok Obat', // Table column heading
            'type'  => 'number',
            // 'prefix'        => 'Rp.',
            // 'suffix'        => ' EUR',
            // 'decimals'      => 2,
            // 'dec_point'     => ',',
            'thousands_sep' => '.',
            // decimals, dec_point and thousands_sep are used to format the number;
            // for details on how they work check out PHP's number_format() method, they're passed directly to it;
            // https://www.php.net/manual/en/function.number-format.php
        ]);
        $this->crud->addColumn([
            'name'  => 'price', // The db column name
            'label' => 'Harga Obat', // Table column heading
            'type'  => 'number',
            'prefix'        => 'Rp ',
            // 'suffix'        => ' EUR',
            // 'decimals'      => 2,
            // 'dec_point'     => ',',
            'thousands_sep' => '.',
            // decimals, dec_point and thousands_sep are used to format the number;
            // for details on how they work check out PHP's number_format() method, they're passed directly to it;
            // https://www.php.net/manual/en/function.number-format.php
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MedicineRequest::class);

        CRUD::field('name');
        $this->crud->addField([   // Number
            'name' => 'stock',
            'label' => 'Stok Obat',
            'type' => 'number',

            // optionals
            // 'attributes' => ["step" => "any"], // allow decimals
            // 'prefix'     => "Rp.",
            // 'suffix'     => ".00",
        ]);
        $this->crud->addField([   // Number
            'name' => 'price',
            'label' => 'Harga Obat',
            'type' => 'number',

            // optionals
            // 'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp.",
            // 'suffix'     => ".00",
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
