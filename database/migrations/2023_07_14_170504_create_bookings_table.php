<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->integer('pasien_id')->nullable();
            $table->string('pasien_name')->nullable();
            $table->string('pasien_email')->nullable();
            $table->integer('category_id')->nullable();
            $table->dateTime('booking_time')->nullable();
            $table->text('complaint')->nullable();
            $table->boolean('confirmed')->nullable()->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
