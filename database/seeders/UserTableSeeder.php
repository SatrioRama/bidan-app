<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'bidan',
            'email' => 'admin@bidan.com',
            'password' => bcrypt('admin123'),
            'type' => 'bidan',
        ]);
    }
}
