@php
    $medicines = \App\Models\Medicine::all();
@endphp

@extends(backpack_view('blank'))

@section('header')
<section class="container-fluid d-print-none">
    <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
    <h2>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.edit')).' '.$crud->entity_name !!}</small>
        @if ($crud->hasAccess('list'))
          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
        @endif
    </h2>
</section>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header">Edit Consultation</div>
                <div class="card-body">
                    <form action="{{ route('consultation.update', $crud->entry->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="booking_id">Booking ID</label>
                            <select name="booking_id" id="booking_id" class="form-control">
                                <option value="">No Booking</option>
                                @foreach(\App\Models\Booking::get() as $booking)
                                    <option value="{{ $booking->id }}" {{ $crud->entry->booking_id == $booking->id ? 'selected' : '' }}>
                                        {{ $booking->pasien->email }} - {{ date('d-m-Y H:i', strtotime($booking->booking_time)) }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="pasien_email">Pasien Email</label>
                            <input type="text" name="pasien_email" id="pasien_email" class="form-control" value="{{ $crud->entry->pasien_email }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="category_id">Category</label>
                            <select name="category_id" id="category_id" class="form-control" required>
                                <option value="" disabled>Select Category</option>
                                @foreach(\App\Models\Category::get() as $category)
                                    <option value="{{ $category->id }}" {{ $crud->entry->category_id == $category->id ? 'selected' : '' }}>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="initial_diagnosis">Initial Diagnosis</label>
                            <input type="text" name="initial_diagnosis" id="initial_diagnosis" class="form-control" value="{{ $crud->entry->initial_diagnosis }}">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control" rows="4">{{ $crud->entry->description }}</textarea>
                        </div>

                        <div id="medicineInputs">
                            @foreach($crud->entry->medicines as $index => $medicine)
                            <div class="medicine-group form-row mb-2">
                                <div class="col-md-8">
                                    <label for="medicine">Medicine</label>
                                    <select name="medicines[{{ $index }}]" class="form-control">
                                        <option value="">Select Medicine</option>
                                        @foreach($medicines as $med)
                                        <option value="{{ $med->id }}" {{ $med->id == $medicine->id ? 'selected' : '' }}>
                                            {{ $med->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="quantity">Quantity</label>
                                    <input type="number" name="quantities[{{ $index }}]" class="form-control" value="{{ $medicine->pivot->quantity }}">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-danger btn-sm mt-4 remove-btn">&times;</button>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <button type="button" class="btn btn-primary btn-sm mb-2" id="addMedicineBtn">Add Medicine</button>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Update Consultation
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after_scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    // Function to handle the change event of the booking select element
    function handleBookingChange() {
        var bookingSelect = document.getElementById('booking_id');
        var pasienEmailInput = document.getElementById('pasien_email');

        // Get the selected booking option
        var selectedBookingOption = bookingSelect.options[bookingSelect.selectedIndex];

        // Check if a booking is selected and update the pasien email input
        if (selectedBookingOption.value !== '') {
            var pasienEmail = selectedBookingOption.text.split(' - ')[0];
            pasienEmailInput.value = pasienEmail;
            pasienEmailInput.readOnly = true;
        } else {
            pasienEmailInput.value = '';
            pasienEmailInput.readOnly = false;
        }
    }

    // Add event listener to the booking select element
    var bookingSelect = document.getElementById('booking_id');
    bookingSelect.addEventListener('change', handleBookingChange);

    // Trigger the initial event to handle the default value
    handleBookingChange();

    // Function to add a new medicine input field
    function createMedicineInput() {
        var medicines = @json($medicines);

        var medicineOptions = '<option value="">Select Medicine</option>';
        var startIndex = $('#medicineInputs .medicine-group').length;
        for (var i = startIndex; i < medicines.length; i++) {
            var medicine = medicines[i];
            medicineOptions += '<option value="' + medicine.id + '">' + medicine.name + '</option>';
        }

        var newInput = `
            <div class="form-row mb-2">
                <div class="col-md-8">
                    <label for="medicine">Medicine</label>
                    <select name="medicines[]" class="form-control">
                        ${medicineOptions}
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="quantity">Quantity</label>
                    <input type="text" name="quantities[]" class="form-control">
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-danger btn-sm mt-4 remove-btn">&times;</button>
                </div>
            </div>
        `;

        $('#medicineInputs').append(newInput);
    }

    // Add event listener to the add medicine button
    $('#addMedicineBtn').on('click', createMedicineInput);

    // Function to handle click event of remove button
    function removeMedicineInput(event) {
        $(event.target).closest('.form-row').remove();
    }

    // Add event listener to handle click event of remove buttons
    $(document).on('click', '.remove-btn', removeMedicineInput);
</script>
@endsection
