<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
        <style>
            @media print {
                #printPageButton {
                    display: none;
                }
                #backButton {
                    display: none;
                }
            }
        </style>
        <title>{{$consultation->pasien_email}}</title>
    </head>
    <body>

    <button id="backButton" onClick="window.history.back();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>KEMBALI</strong></button>
    <br>
    <br>
    <button id="printPageButton" onClick="window.print();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>PRINT</strong></button>

    <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="font-size: 200%"><center>Bidan Eva Latifa</center></td>
            </tr>
            <tr>
                <td><center>Cinangka Depok Barat</center></td>
            </tr>
        </table>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="text-align: left">{{date('d-m-Y',strtotime(@$consultation->created_at))}}</td>
                <td style="text-align: right">{{date('H:i:s',strtotime(@$consultation->created_at))}}</td>
            </tr>
        </table>
        <hr>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="width: 45%">Nama Pasien</td>
                <td style="width: 5%">:</td>
                <td style="width: 50%">{{@$consultation->pasien->name}}</td>
            </tr>
            <tr>
                <td style="width: 45%">Email Pasien</td>
                <td style="width: 5%">:</td>
                <td style="width: 50%">{{@$consultation->pasien_email}}</td>
            </tr>
        </table>
        <hr>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="width: 45%">Diagnosa Awal</td>
                <td style="width: 5%">:</td>
                <td style="width: 50%">{{@$consultation->initial_diagnosis}}</td>
            </tr>
            <tr>
                <td style="width: 45%">Full Diagnosa</td>
                <td style="width: 5%">:</td>
                <td style="width: 50%">{{@$consultation->description}}</td>
            </tr>
            <tr>
                <td style="width: 45%">Harga Pengobatan</td>
                <td style="width: 5%">:</td>
                <td style="width: 50%">Rp. {{number_format($consultation->category->price, 0, ',', '.')}}</td>
            </tr>
        </table>
        <hr>
        <table border="1" style="width: 100%">
            <tr>
                <td style="width: 45%">Obat</td>
                <td style="width: 5%">Qty</td>
                <td style="width: 50%">Harga</td>
            </tr>
            @php
                $total_med = 0;
            @endphp
            @foreach ($consultation->medicines as $medicine)
                <tr>
                    <td>{{$medicine->name}}</td>
                    <td>{{$medicine->pivot->quantity}}</td>
                    <td>Rp. {{number_format($medicine->pivot->quantity*$medicine->price, 0, ',', '.')}}</td>
                </tr>
                @php
                    $total_med += $medicine->pivot->quantity*$medicine->price;
                @endphp
            @endforeach
        </table>
        <hr>
        <table border="1" style="width: 100%">
            <tr>
                <td style="width: 50%">Deskripsi</td>
                <td style="width: 50%">Total harga</td>
            </tr>
            <tr>
                <td style="width: 50%">Konsultasi</td>
                <td style="width: 50%">Rp. {{number_format($consultation->category->price, 0, ',', '.')}}</td>
            </tr>
            <tr>
                <td style="width: 50%">Obat</td>
                <td style="width: 50%">Rp. {{number_format($total_med, 0, ',', '.')}}</td>
            </tr>
            <tr>
                <td style="width: 50%">Total</td>
                <td style="width: 50%">Rp. {{number_format($total_med+$consultation->category->price, 0, ',', '.')}}</td>
            </tr>
        </table>
        <br>
        <table cellspacing="0" border="0" style="width: 100%; text-align: center">
            <tr>
                <td>
                    Lekas Sembuh
                    <br>
                    * * Thank You * *
                </td>
            </tr>
            {{-- <tr>
                <td><i class="las la-globe-americas"></i> www.alcrace.com</td>
            </tr>
            <tr>
                <td><i class="lab la-instagram"></i> &commat;alcraceofficial</td>
            </tr> --}}
        </table>
    </body>
</html>
