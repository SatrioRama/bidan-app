@extends(backpack_view('blank'))

@section('header')
<section class="container-fluid d-print-none">
    <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
    <h2>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.edit')).' '.$crud->entity_name !!}</small>
        @if ($crud->hasAccess('list'))
          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
        @endif
    </h2>
</section>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">Consultation Details</div>
                    <div class="card-body">
                        @if (!empty($crud->entry->booking_id))
                            <div class="form-group">
                                <label for="booking_id">Booking</label>
                                <input type="text" id="booking_id" class="form-control" value="{{ $crud->entry->booking->pasien_name }} - {{$crud->entry->booking->booking_time}}" readonly>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="pasien_email">Pasien Email</label>
                            <input type="text" id="pasien_email" class="form-control" value="{{ $crud->entry->pasien_email }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="category_id">Category</label>
                            <input type="text" id="category_id" class="form-control" value="{{ $crud->entry->category->name }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="initial_diagnosis">Initial Diagnosis</label>
                            <input type="text" id="initial_diagnosis" class="form-control" value="{{ $crud->entry->initial_diagnosis }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" class="form-control" rows="4" readonly>{{ $crud->entry->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="medicines">Medicines</label>
                            <ul>
                                @foreach($crud->entry->medicines as $medicine)
                                    <li>{{ $medicine->name }} - Quantity: {{ $medicine->pivot->quantity }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url($crud->route) }}" class="btn btn-secondary mt-3">
                            Back to List
                        </a>
                        <a href="{{ route('print.receipt', $crud->entry->id) }}" class="btn btn-primary mt-3" style="float: right">
                            Print Receipt
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
