@extends(backpack_view('blank'))

@section('content')
<div class="container">
    <h1 class="text-center">Report</h1>
    <div class="mb-3">
        <label for="dateRangePicker" class="form-label">Date Range Filter:</label>
        <input type="text" id="dateRangePicker" class="form-control">
    </div>
    <hr>
    <div class="table-responsive">
        <table id="patientsTable" class="table table-bordered responsive-">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pasien</th>
                    <th>Phone</th>
                    <th>Email Pasien</th>
                    <th>Tanggal</th>
                    <th>Konsultasi</th>
                    <th>Diagnosa</th>
                    <th>Deskripsi</th>
                    <th>Obat</th>
                    <th>harga</th>
                </tr>
            </thead>
            <tbody>
                <!-- Loop through your patients data and create rows for each patient -->
                @foreach ($data as $index => $consultation)
                    @php
                        $patientFound = !is_null($consultation->pasien);
                    @endphp
                    <tr class="{{ $patientFound ? 'bg-success' : 'bg-warning' }}">
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $patientFound ? $consultation->pasien->name : 'pasien baru' }}</td>
                        <td>{{ $patientFound ? $consultation->pasien->phone : 'pasien baru' }}</td>
                        <td>{{ $consultation->pasien_email }}</td>
                        <td>{{ date('d-m-Y', strtotime($consultation->created_at)) }}</td>
                        <td>{{ $consultation->category->name }}</td>
                        <td>{{ $consultation->initial_diagnosis }}</td>
                        <td>{{ $consultation->description }}</td>
                        <td>
                            @php
                                $price_med = 0;
                            @endphp
                            @foreach ($consultation->medicines as $medicine)
                                {{$medicine->name}}
                                @if (!$loop->last)
                                    ,
                                @endif
                                @php
                                    $price_med += $medicine->pivot->quantity*$medicine->price;
                                @endphp
                            @endforeach
                        </td>
                        <td>Rp. {{number_format($price_med+$consultation->category->price, 0, ',', '.')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('after_styles')
<link href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.css" rel="stylesheet">
<style>
    .bg-success {
        background-color: green;
    }

    .bg-warning {
        background-color: yellow;
    }
</style>
@endsection

@section('after_scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js"></script>

    <script>
        $(document).ready(function() {
            // Convert the dates in the DataTable to the correct format (DD-MM-YYYY to YYYY-MM-DD)
            $('#patientsTable tbody tr').each(function() {
                const dateCell = $(this).find('td:eq(4)'); // Assuming the date column is at index 4
                const dateText = dateCell.text();
                const dateParts = dateText.split('-');
                const newDateFormat = `${dateParts[2]}-${dateParts[1]}-${dateParts[0]}`;
                dateCell.text(newDateFormat);
            });

            // Initialize the DataTable
            const table = $('#patientsTable').DataTable();

            // Add the date range filter using daterangepicker
            $('#dateRangePicker').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            // Apply the filter when the date range is changed
            $('#dateRangePicker').on('apply.daterangepicker', function(ev, picker) {
                const startDate = picker.startDate.format('YYYY-MM-DD');
                const endDate = picker.endDate.format('YYYY-MM-DD');

                table.column(4).search(`${startDate}|${endDate}`, true, false).draw();
            });

            // Clear the filter when the "Clear" button is clicked
            $('#dateRangePicker').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                table.column(4).search('').draw();
            });
        });
    </script>
@endsection

