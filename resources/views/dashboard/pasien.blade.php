@extends(backpack_view('blank'))

@section('content')
<div class="container">
    <h1 class="text-center">Rekam Medis</h1>
    <hr>
    <table>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{@backpack_user()->name}}</td>
        </tr>
        <tr>
            <td>Phone</td>
            <td>:</td>
            <td>{{@backpack_user()->phone}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{@backpack_user()->email}}</td>
        </tr>
        <tr>
            <td>Kunjungan</td>
            <td>:</td>
            <td>{{@count($data)}}</td>
        </tr>
    </table>
    <table id="patientsTable" class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Konsultasi</th>
                <th>Diagnosa</th>
                <th>Deskripsi</th>
                <th>Obat</th>
                <th>harga</th>
            </tr>
        </thead>
        <tbody>
            <!-- Loop through your patients data and create rows for each patient -->
            @foreach ($data as $index => $consultation)
                @php
                    $patientFound = !is_null($consultation->pasien);
                @endphp
                <tr class="{{ $patientFound ? 'bg-success' : 'bg-warning' }}">
                    <td>{{ $index + 1 }}</td>
                    <td>{{ date('d-m-Y', strtotime($consultation->created_at)) }}</td>
                    <td>{{ $consultation->category->name }}</td>
                    <td>{{ $consultation->initial_diagnosis }}</td>
                    <td>{{ $consultation->description }}</td>
                    <td>
                        @php
                            $price_med = 0;
                        @endphp
                        @foreach ($consultation->medicines as $medicine)
                            {{$medicine->name}}
                            @if (!$loop->last)
                                ,
                            @endif
                            @php
                                $price_med += $medicine->pivot->quantity*$medicine->price;
                            @endphp
                        @endforeach
                    </td>
                    <td>Rp. {{number_format($price_med+$consultation->category->price, 0, ',', '.')}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('after_styles')
<link href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<style>
    .bg-success {
        background-color: green;
    }

    .bg-warning {
        background-color: yellow;
    }
</style>
@endsection

@section('after_scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

    <script>
        $(document).ready(function() {
            // Initialize the DataTable
            $('#patientsTable').DataTable();
        });
    </script>
@endsection
