@extends(backpack_view('layouts.plain'))

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-4">
            <h3 class="text-center mb-4 mt-3">
                <img src="{{asset('logo.png')}}" style="width:240px;height:auto;">
            </h3>
            <div class="card">
                <div class="card-body">
                    <form class="col-md-12 p-t-10" role="form" method="POST" action="{{ route('backpack.auth.register1') }}">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="control-label" for="name">{{ trans('backpack::base.name') }}</label>

                            <div>
                                <input type="text" class="form-control{{ (!empty($errors)) ? $errors->has('name') ? ' is-invalid' : '' : '' }}" name="name" value="{{ old('name') }}" id="name">

                                @if ((!empty($errors)) ? $errors->has('name') : '')
                                    <span class="invalid-feedback">
                                        <strong>{{ (!empty($errors)) ? $errors->first('name')  : '' }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="email">{{ trans('backpack::base.email_address') }}</label>

                            <div>
                                <input type="email" class="form-control{{ (!empty($errors)) ? $errors->has('email') ? ' is-invalid' : '' : '' }}" name="email" value="{{ old('email') }}" id="email">

                                @if ((!empty($errors)) ? $errors->has('email')  : '')
                                    <span class="invalid-feedback">
                                        <strong>{{ (!empty($errors)) ? $errors->first('email')  : '' }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">{{ trans('backpack::base.password') }}</label>

                            <div>
                                <input type="password" class="form-control{{ (!empty($errors)) ? $errors->has('password') ? ' is-invalid' : '' : '' }}" name="password" id="password">

                                @if ((!empty($errors)) ? $errors->has('password')  : '')
                                    <span class="invalid-feedback">
                                        <strong>{{ (!empty($errors)) ? $errors->first('password')  : '' }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password_confirmation">{{ trans('backpack::base.confirm_password') }}</label>

                            <div>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="phone">Phone</label>

                            <div>
                                <input type="text" class="form-control{{ (!empty($errors)) ? $errors->has('phone') ? ' is-invalid' : '' : '' }}" name="phone" value="{{ old('phone') }}" id="phone" pattern="[0-9]*" minlength="9" maxlength="15" required>

                                @if ((!empty($errors)) ? $errors->has('phone')  : '' )
                                    <span class="invalid-feedback">
                                        <strong>{{ (!empty($errors)) ? $errors->first('phone')  : '' }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ trans('backpack::base.register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <a href="{{ route('backpack.auth.login') }}">{{ trans('backpack::base.login') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
