<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
@if (backpack_user()->type == 'bidan')
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la la-braille'></i> Categories</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('medicine') }}'><i class='nav-icon la la-capsules'></i> Medicines</a></li>
@endif

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('booking') }}'><i class='nav-icon la la-book-medical'></i> Bookings</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('consultation') }}'><i class='nav-icon la la-notes-medical'></i> Consultations</a></li>
