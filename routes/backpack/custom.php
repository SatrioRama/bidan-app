<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.
Route::get('admin/register', 'App\Http\Controllers\Admin\Auth\RegisterController@showRegistrationForm')->name('backpack.auth.register');
Route::post('admin/register', 'App\Http\Controllers\Admin\Auth\RegisterController@register')->name('backpack.auth.register1');

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::get('dashboard', 'AdminController@dashboard')->name('backpack.dashboard');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('medicine', 'MedicineCrudController');
    Route::crud('booking', 'BookingCrudController');
    Route::crud('consultation', 'ConsultationCrudController');
    Route::get('/print-receipt/{id}', 'ConsultationCrudController@printReceipt')->name('print.receipt');
}); // this should be the absolute last line of this file
